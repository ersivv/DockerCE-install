# Install packages to allow apt to use a repository over HTTPS
apt-get install -y \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg2 \
     software-properties-common
     
# Add Docker’s official GPG key:
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -

# Use the following command to set up the stable repository
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"
   
# Update the apt package index
apt-get update

# Install the latest version of Docker
apt-get install docker-ce

# Verify that Docker CE is installed correctly by running the hello-world image.
docker run hello-world

# Install latest Docker Compose (https://github.com/docker/compose/releases)
curl -L https://github.com/docker/compose/releases/download/1.14.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

# Check version Docker Compose
docker-compose -v

# Install Simple Docker UI is an unofficial developer tool for monitoring and managing your Docker containers (https://github.com/felixgborrego/simple-docker-ui)
apt-get -f install

cd /tmp
wget https://github.com/felixgborrego/simple-docker-ui/releases/download/v0.5.5/SimpleDockerUI_0.5.5_amd64.deb
dpkg -i SimpleDockerUI_0.5.5_amd64.deb
rm SimpleDockerUI_0.5.5_amd64.deb

echo [Unit] >> /etc/systemd/system/docker-tcp.socket
echo Description=Docker HTTP Socket for the API
echo >> /etc/systemd/system/docker-tcp.socket
echo [Socket] >> /etc/systemd/system/docker-tcp.socket
echo ListenStream=2375 >> /etc/systemd/system/docker-tcp.socket
echo BindIPv6Only=both >> /etc/systemd/system/docker-tcp.socket
echo Service=docker.service >> /etc/systemd/system/docker-tcp.socket
echo  >> /etc/systemd/system/docker-tcp.socket
echo [Install] >> /etc/systemd/system/docker-tcp.socket
echo WantedBy=sockets.target >> /etc/systemd/system/docker-tcp.socket

systemctl enable docker-tcp.socket
systemctl stop docker
systemctl start docker-tcp.socket